---
title: Pre-requisites
weight: 1
url: install/prerequisites
description: Details about the requirements to set up the operator.
---

As explained in the [Demo section]({{% relref "02-demo-quickstart/01-setting-up-the-environment/_index.md" %}}), for setting up the StackGres operator and clusters, you need a running Kubernetes environment.

Starting from version 1.2.x, StackGres is able to run on any Kubernetes installation from version 1.18 to 1.25.

{{% children style="li" depth="1"  description="true" %}}
