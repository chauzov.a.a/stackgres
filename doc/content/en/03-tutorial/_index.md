---
title: Tutorial
weight: 3
chapter: true
url: tutorial
---

### Chapter 3

# Tutorial

Hands on Laboratory, to discover and practice the most significant StackGres features with guided examples:


{{% children style="li" depth="1" description="true" %}}


## Prerequisites

To run this tutorial, you will need to have installed in your environment:

* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/), the Kubernetes CLI.
* [Helm 3](https://helm.sh/docs/intro/install/). Helm v3 is required.
* Some commands may require running them from a `shell`.
